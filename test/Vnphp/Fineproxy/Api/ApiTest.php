<?php

class ApiTest extends \PHPUnit_Framework_TestCase
{
    protected $login = 'login';

    protected $password = 'password';

    public function testCanCreate()
    {
        $instance = new \Vnphp\Fineproxy\Api\Api($this->login, $this->password);
        static::assertInstanceOf(\Vnphp\Fineproxy\Api\Api::class, $instance);
    }
}
