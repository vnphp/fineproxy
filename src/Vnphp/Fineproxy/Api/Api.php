<?php


namespace Vnphp\Fineproxy\Api;

class Api implements ApiInterface
{
    protected $login;

    protected $password;

    protected $proxies;

    /**
     * Api constructor.
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    public function getProxies()
    {
        if (null === $this->proxies) {
            $apiUrl = strtr('http://account.fineproxy.org/api/getproxy/?format=txt&type=httpauth&login=YOUR_LOGIN&password=YOUR_PASS', [
                'YOUR_LOGIN' => $this->login,
                'YOUR_PASS'  => $this->password,
            ]);
            $res = file_get_contents($apiUrl);
            $proxies = explode("\n", $res);
            $proxies = array_map('trim', $proxies);
            $proxies = array_filter($proxies);
            $this->proxies = array_map(function ($proxy) {
                return "{$this->login}:{$this->password}@$proxy";
            }, $proxies);
        }
        return $this->proxies;
    }
}
