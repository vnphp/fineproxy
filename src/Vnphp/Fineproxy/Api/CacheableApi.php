<?php


namespace Vnphp\Fineproxy\Api;

use Doctrine\Common\Cache\Cache;

class CacheableApi implements ApiInterface
{
    /**
     * @var ApiInterface
     */
    protected $delagate;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var integer
     */
    protected $ttl;

    /**
     * CacheableApi constructor.
     * @param ApiInterface $delagate
     * @param Cache $cache
     * @param int $ttl
     */
    public function __construct(ApiInterface $delagate, Cache $cache, $ttl = 60)
    {
        $this->delagate = $delagate;
        $this->cache = $cache;
        $this->ttl = $ttl;
    }

    public function getProxies()
    {
        $cacheKey = md5(__METHOD__);
        $proxies = $this->cache->fetch($cacheKey);
        if (false === $proxies) {
            $proxies = $this->delagate->getProxies();
            $this->cache->save($cacheKey, $proxies, $this->ttl);
        }
        return $proxies;
    }
}
