<?php


namespace Vnphp\Fineproxy\Api;

interface ApiInterface
{
    public function getProxies();
}
