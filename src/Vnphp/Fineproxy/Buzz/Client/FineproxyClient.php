<?php


namespace Vnphp\Fineproxy\Buzz\Client;

use Buzz\Client\Curl;
use Buzz\Exception\RequestException;
use Buzz\Message\MessageInterface;
use Buzz\Message\RequestInterface;
use Vnphp\Fineproxy\Api\ApiInterface;

class FineproxyClient extends Curl
{
    /**
     * @var ApiInterface
     */
    protected $api;

    protected $failCount = 0;

    protected $maxFails = 10;

    /**
     * FineproxyClient constructor.
     * @param ApiInterface $api
     */
    public function __construct(ApiInterface $api)
    {
        $this->api = $api;
        parent::__construct();
    }

    public function send(RequestInterface $request, MessageInterface $response, array $options = [])
    {
        $this->initProxy();

        try {
            parent::send($request, $response, $options);
        } catch (RequestException $e) {
            ++$this->failCount;
            if ($this->failCount >= $this->maxFails) {
                throw $e;
            }
            $this->send($request, $response, $options);
        }


        $this->failCount = 0;
    }

    /**
     * @param int $maxFails
     */
    public function setMaxFails($maxFails)
    {
        $this->maxFails = $maxFails;
    }

    protected function initProxy()
    {
        $proxies = $this->api->getProxies();
        shuffle($proxies);
        $proxy = $proxies[array_rand($proxies)];
        $this->setProxy($proxy);
    }
}
