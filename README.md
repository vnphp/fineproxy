# Vnphp Fineproxy

Unofficial api client for http://fineproxy.org/.


[![build status](https://gitlab.com/vnphp/fineproxy/badges/master/build.svg)](https://gitlab.com/vnphp/fineproxy/commits/master)

## Installation 

```
composer require vnphp/fineproxy
```

You need to have `git` installed. 

## Usage

```php
<?php

$api = new \Vnphp\Fineproxy\Api\Api($login, $password);
$proxies = $api->getProxies();
```
